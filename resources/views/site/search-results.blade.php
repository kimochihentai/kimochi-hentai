@extends('layouts.site')
@section('body_id', '')
@section('body_class', 'site gallery')
@section('content')
<div class="page-content">
	<div class="gallery">
		@if($empty)
		<div class="row">
			<div class="columns">
				<h3>Well, that's awkward. There don't seem to be any relevant posts here... yet...</h3>
			</div>
		</div>
		@else
		<div class="row">
			<div class="columns">
				<div class="image-grid">
					<div class="row collapse small-up-2 medium-up-3 large-up-4">
						@if($images)
							@foreach($images as $image)
							<div class="columns">
								<div class="image-tile-container">
									<a href="/view-post/{{ $image->id }}">
										<div class="image-tile" style="background-image: url('<?php echo asset('uploads/' . $image->year . '/' . $image->month . '/'. $image->user_id . '/' . $image->id . '.' . $image->file_extension) ?>');"></div>
										<div class="image-text">
											<span class="title">{{ $image->title }}</span>
											<span class="author">{{ $image->author }}</span>
											<p class="desc">{{ $image->description }}</p>
										</div>
									</a>
								</div>
							</div>
							@endforeach
						@endif
						@if($results_by_tag)
							@foreach($results_by_tag as $image)
							<div class="columns">
								<div class="image-tile-container">
									<a href="/view-post/{{ $image->id }}">
										<div class="image-tile" style="background-image: url('<?php echo asset('uploads/' . $image->year . '/' . $image->month . '/'. $image->user_id . '/' . $image->id . '.' . $image->file_extension) ?>');"></div>
										<div class="image-text">
											<span class="title">{{ $image->title }}</span>
											<span class="author">{{ $image->author }}</span>
											<p class="desc">{{ $image->description }}</p>
										</div>
									</a>
								</div>
							</div>
							@endforeach
						@endif
						@if($results_by_title)
							@foreach($results_by_title as $image)
							<div class="columns">
								<div class="image-tile-container">
									<a href="/view-post/{{ $image->id }}">
										<div class="image-tile" style="background-image: url('<?php echo asset('uploads/' . $image->year . '/' . $image->month . '/'. $image->user_id . '/' . $image->id . '.' . $image->file_extension) ?>');"></div>
										<div class="image-text">
											<span class="title">{{ $image->title }}</span>
											<span class="author">{{ $image->author }}</span>
											<p class="desc">{{ $image->description }}</p>
										</div>
									</a>
								</div>
							</div>
							@endforeach
						@endif
						@if($results_by_user)
							@foreach($results_by_user as $user)
							<div class="columns">
								<div class="image-tile-container">
									<a href="/user/{{ $user->id }}">
										<?php
											$profile_picture_path = '/profile_pictures/placeholder.png';
											if(file_exists(public_path() . '/profile_pictures/' . $top_artist->id))
											{
												if(sizeof(scandir(public_path() . '/profile_pictures/' . $top_artist->id)) >= 3)
												{
													$profile_picture_path = '/profile_pictures/' . $top_artist->id . '/' . scandir(public_path() . '/profile_pictures/' . $top_artist->id)[2];
												}
											}
										?>
										<div class="image-tile" style="background-image: url('<?php echo $profile_picture_path ?>');"></div>
										<div class="image-text">
											<span class="title">{{ $user->name }}</span>
											<p class="desc">{{ $user->bio }}</p>
										</div>
									</a>
								</div>
							</div>
							@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>
		@endif
	</div>
</div>
<script src="/js/home.js" defer></script>
@endsection