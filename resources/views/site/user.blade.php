@extends('layouts.site')
@section('body_id', 'user-page')
@section('body_class', 'site')
@section('content')
<div class="page-content">
	<div class="row">
		<div class="columns">
			<div class="profile-header">
				<div class="row">
					<div class="columns shrink">
						<div class="total-likes">
							<i class="fas fa-heart"></i>
							@if($user_details->user->likes)
								<span>{{ $user_details->user->likes }}</span>
							@else
								<span>0</span>
							@endif
						</div>
						<?php 
						$picture_link = asset('profile_pictures/' . $user_details->user->id . '/' . $user_details->profile_picture);
						?>
						@if($user_details->profile_picture != 0)
							<div class="profile-picture easy-edit-container" style="background-image: url('{{ $picture_link }}')"></div>
						@else
							<div class="profile-picture easy-edit-container" style="background-image: url('<?php echo asset('/profile_pictures/placeholder.png') ?>')"></div>
						@endif
						<div class="name">{{ $user_details->user->name }}</div>
						<div class="social-links">
							<!-- Patreon -->
							<div class="social-icon easy-edit-container">
								<a href="{{ $user_details->user->social_link_patreon }}" target="_blank">
									<i class="fab fa-patreon"></i>
								</a>
							</div>
							<!-- Twitter -->
							<div class="social-icon easy-edit-container">
								<a href="{{ $user_details->user->social_link_twitter }}" target="_blank">
									<i class="fab fa-twitter"></i>
								</a>
							</div>
							<!-- Facebook -->
							<div class="social-icon easy-edit-container">
								<a href="{{ $user_details->user->social_link_facebook }}" target="_blank">
									<i class="fab fa-facebook"></i>
								</a>
							</div>
						</div>
					</div>
					<div class="columns">
						<div class="profile-nav">
							<div class="row">
								<div class="columns">
									<a href="#" class="active" tabFor="1">Uploads</a>
								</div>
								<div class="columns">
									<a href="#" tabFor="2">Favourites</a>
								</div>
								<div class="columns">
									<a href="#" tabFor="3">Biography</a>
								</div>
								<div class="columns">
									<a href="#" tabFor="4">Report User</a>
								</div>
							</div>
						</div>
						<div class="profile-nav-content">
							<div class="content-tab active" forTab="1">
								@if($uploads->count())
									<div class="content-block images pop-in active">
										<div class="image-grid">
											<div class="row collapse small-up-2 medium-up-3 large-up-3">
												@foreach($uploads as $upload)
												<div class="columns">
													<div class="image-tile-container">
														<a href="/view-post/{{ $upload->id }}">
															<div class="image-tile" style="background-image: url('<?php echo asset('uploads/' . $upload->year . '/' . $upload->month . '/'. $upload->user_id . '/' . $upload->id . '.' . $upload->file_extension) ?>');"></div>
															<div class="image-text">
																<span class="title">{{ $upload->title }}</span>
																<span class="author">{{ $upload->author }}</span>
																<p class="desc">{{ $upload->description }}</p>
															</div>
														</a>
													</div>
												</div>
												@endforeach
											</div>
										</div>
									</div>
								@else
									<div class="row">
										<div class="columns">
											<span class="message">No saucy uploads just yet...</span>
										</div>
									</div>
								@endif
							</div>
							<div class="content-tab" forTab="2">
								@if($liked_posts->count())
									<div class="content-block images pop-in">
										<div class="image-grid">
											<div class="row collapse small-up-2 medium-up-3 large-up-3">
												@foreach($liked_posts as $upload)
												<div class="columns">
													<div class="image-tile-container">
														<a href="/view-post/{{ $upload->id }}">
															<div class="image-tile" style="background-image: url('<?php echo asset('uploads/' . $upload->year . '/' . $upload->month . '/'. $upload->user_id . '/' . $upload->id . '.' . $upload->file_extension) ?>');"></div>
															<div class="image-text">
																<span class="title">{{ $upload->title }}</span>
																<span class="author">{{ $upload->author }}</span>
																<p class="desc">{{ $upload->description }}</p>
															</div>
														</a>
													</div>
												</div>
												@endforeach
											</div>
										</div>
									</div>
								@else
									<div class="row">
										<div class="columns">
											<span class="message">This user is yet to smash that like button...</span>
										</div>
									</div>
								@endif
							</div>
							<div class="content-tab" forTab="3">
								<p class="profile-bio">
									@if($user_details->user->bio)
									<p class="bio">
										{{ $user_details->user->bio }}
									</p>
									@else
									<span class="message">No cool info about this user has been established at present.</span>
									@endif
								</p>
							</div>
							<div class="content-tab" forTab="4">
								<div class="row">
									<div class="columns">
										@if(!$reported)
										<span class="message">Too cheeky? If you'd like to report this user, click the button below and we'll look into it.</span>
										<a href="/report-user/{{ $user_details->user->id }}" class="button report">Report</a>
										@else
										<span class="message">Thanks for letting us know you'd like to report this user, we're on it! (If you reported them by accident, don't worry, it's chill. Users deserving of a report will not be punished until an investigation is conducted)</span>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="/js/home.js" defer></script>
@endsection