@extends('layouts.site')
@section('body_id', 'user-page')
@section('body_class', 'site')
@section('content')
<div class="confirm-delete-post-overlay">
	<div class="inner">
		<span>Are you reaaally sure you wanna delete this post? You probably are, but you know, gotta ask.</span>
		<div class="buttons">
			<a href="#" class="button confirm">Yes</a>
			<a href="#" class="button cancel">No</a>
		</div>
	</div>
</div>
<div class="upload-post-overlay">
	<div class="close-overlay-button">
		<div class="line"></div>
		<div class="line"></div>
	</div>
	<div class="inner">
		<div class="row">
			<div class="columns">
				<h2>Upload a new post</h2>
				<span class="rules">The <strong>rules</strong> are <strong>simple</strong>. If you think the contents of the upload could, in some <strong>rational</strong> way, endanger the legal wellbeing of this website, it probably will. And well, this is a <strong>hentai</strong> site, so keep it hentai. We encourage <strong>cosplay</strong>, <strong>sculpting</strong>, and other unconventional hentai media that requires uploading real life photo imagery. With <strong>great power</strong> also comes <strong>great</strong> <span class="strikethrough">responsibility</span><strong> legal obligation</strong>. <strong>NO</strong> photos or content depicting <strong>child porn</strong>, <strong>beastiality</strong> or <strong>otherwise generally illegal or hateful subject matter, such as blatant racism or subject matter clearly intending to incite violence or hatred</strong>. You get the idea, now have fun and thank you for uploading and supporting <strong>Kimochi Hentai</strong>!</span>
				<form action="{{ route('upload-post') }}" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="form-row">
						<div>
							<span class="number">1.</span><label for="">Please select a file that you wish to upload</label>
							<input type="file" name="upload" required accept="image/x-png,image/gif,image/jpeg,image/png">
						</div>
					</div>
					<div class="form-row">
						<div>
							<span class="number">2.</span><label for="">What would you like to call this one?</label>
							<input type="text" name="title" required placeholder="Let's keep it SFW... I mean, uh... NSFW?...">
						</div>
					</div>
					<div class="form-row">
						<div>
							<span class="number">3.</span><label for="">What should we categorize this post under?</label>
							<div class="tag-list">
								@foreach($tags as $tag)
									<div class="tag-item">
										<div class="icon-container">
											<i class="fas fa-check"></i>
										</div>
										<input type="checkbox" name="tags[]" value="{{ $tag->name }}" id="tag-{{ $tag->name }}">
										<label for="tag-test">{{ $tag->name }}</label>
									</div>
								@endforeach
							</div>
						</div>
					</div>
					<div class="form-row">
						<div>
							<span class="number">4.</span><label for="">Any last words?...</label>
							<textarea name="description" wrap="physical" cols="1" rows="8"></textarea>
						</div>
					</div>
					<button type="submit">submit</button>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="page-content">
	<div class="row">
		<div class="columns">
			<div class="profile-header">
				<div class="row">
					<div class="columns shrink">
						<div class="total-likes">
							<i class="fas fa-heart"></i>
							@if(Auth::user()->likes)
								<span>{{ Auth::user()->likes }}</span>
							@else
								<span>0</span>
							@endif
						</div>
						<?php 
						$picture_link = asset('profile_pictures/' . Auth::user()->id . '/' . $user->profile_picture);
						?>
						@if($user->profile_picture != 0)
							<div class="profile-picture easy-edit-container" style="background-image: url('{{ $picture_link }}')">
						@else
							<div class="profile-picture easy-edit-container" style="background-image: url('<?php echo asset('/profile_pictures/placeholder.png') ?>')">
						@endif
							<button class="easy-edit-button edit-profile-picture"><i class="fas fa-pencil-alt edit-icon"></i><i class="fas fa-times close-icon"></i></button>
							<form action="{{ route('update-profile-picture') }}" method="POST" id="update-profile-picture" enctype="multipart/form-data">
								@csrf
								<input type="file" name="upload" accept="image/x-png,image/gif,image/jpeg,image/png">
								<button type="submit"><i class="fas fa-check"></i></button>
							</form>
						</div>
						<div class="name">{{ Auth::user()->name }}</div>
						<button class="upload">+ Upload</button>
						<div class="social-links">
							<!-- Patreon -->
							<div class="social-icon easy-edit-container">
								<a href="{{ Auth::user()->social_link_patreon }}" target="_blank">
									<i class="fab fa-patreon"></i>
								</a>
								<button class="easy-edit-button">
									<i class="fas fa-pencil-alt edit-icon"></i><i class="fas fa-times close-icon"></i>
								</button>
								<form action="{{ route('update-patreon-link') }}" method="POST">
									@csrf
									<input type="text" name="link-patreon" accept="image/x-png,image/gif,image/jpeg,image/png" placeholder="Update social link">
									<button type="submit"><i class="fas fa-check"></i></button>
								</form>
							</div>
							<!-- Twitter -->
							<div class="social-icon easy-edit-container">
								<a href="{{ Auth::user()->social_link_twitter }}" target="_blank">
									<i class="fab fa-twitter"></i>
								</a>
								<button class="easy-edit-button">
									<i class="fas fa-pencil-alt edit-icon"></i><i class="fas fa-times close-icon"></i>
								</button>
								<form action="{{ route('update-twitter-link') }}" method="POST">
									@csrf
									<input type="text" name="link-twitter" accept="image/x-png,image/gif,image/jpeg,image/png" placeholder="Update social link">
									<button type="submit"><i class="fas fa-check"></i></button>
								</form>
							</div>
							<!-- Facebook -->
							<div class="social-icon easy-edit-container">
								<a href="{{ Auth::user()->social_link_facebook }}" target="_blank">
									<i class="fab fa-facebook"></i>
								</a>
								<button class="easy-edit-button">
									<i class="fas fa-pencil-alt edit-icon"></i><i class="fas fa-times close-icon"></i>
								</button>
								<form action="{{ route('update-facebook-link') }}" method="POST">
									@csrf
									<input type="text" name="link-facebook" accept="image/x-png,image/gif,image/jpeg,image/png" placeholder="Update social link">
									<button type="submit"><i class="fas fa-check"></i></button>
								</form>
							</div>
						</div>
					</div>
					<div class="columns">
						<div class="profile-nav">
							<div class="row">
								<div class="columns">
									<a href="#" class="active" tabFor="1">Uploads</a>
								</div>
								<div class="columns">
									<a href="#" tabFor="2">Liked Posts</a>
								</div>
								<div class="columns">
									<a href="#" tabFor="3">Biography</a>
								</div>
								<div class="columns">
									<a href="#" tabFor="4">History</a>
								</div>
							</div>
						</div>
						<div class="profile-nav-content">
							<div class="content-tab active" forTab="1">
								@if($uploads->count())
									<div class="content-block images pop-in active">
										<div class="image-grid">
											<div class="row collapse small-up-2 medium-up-3 large-up-3">
												@foreach($uploads as $upload)
												<div class="columns">
													<div class="image-tile-container">
														<div class="delete-post-button" forPost="{{ $upload->id }}">
															<div class="line"></div>
															<div class="line"></div>
														</div>
														<a href="/view-post/{{ $upload->id }}">
															<div class="image-tile" style="background-image: url('<?php echo asset('uploads/' . $upload->year . '/' . $upload->month . '/'. $upload->user_id . '/' . $upload->id . '.' . $upload->file_extension) ?>');"></div>
															<div class="image-text">
																<span class="title">{{ $upload->title }}</span>
																<span class="author">{{ $upload->author }}</span>
																<p class="desc">{{ $upload->description }}</p>
															</div>
														</a>
													</div>
												</div>
												@endforeach
											</div>
										</div>
									</div>
								@else
									<div class="row">
										<div class="columns">
											<span class="message">No saucy uploads just yet...</span>
										</div>
									</div>
								@endif
							</div>
							<div class="content-tab" forTab="2">
								@if($liked_posts->count())
									<div class="content-block images pop-in">
										<div class="image-grid">
											<div class="row collapse small-up-2 medium-up-3 large-up-3">
												@foreach($liked_posts as $upload)
												<div class="columns">
													<div class="image-tile-container">
														<a href="/view-post/{{ $upload->id }}">
															<div class="image-tile" style="background-image: url('<?php echo asset('uploads/' . $upload->year . '/' . $upload->month . '/'. $upload->user_id . '/' . $upload->id . '.' . $upload->file_extension) ?>');"></div>
															<div class="image-text">
																<span class="title">{{ $upload->title }}</span>
																<span class="author">{{ $upload->author }}</span>
																<p class="desc">{{ $upload->description }}</p>
															</div>
														</a>
													</div>
												</div>
												@endforeach
											</div>
										</div>
									</div>
								@else
									<div class="row">
										<div class="columns">
											<span class="message">Posts you smash that like button on will appear here!</span>
										</div>
									</div>
								@endif
							</div>
							<div class="content-tab" forTab="3">
								<p class="profile-bio">
									@if(Auth::user()->bio)
									<p class="bio">
										{{ Auth::user()->bio }}
									</p>
									@else
									<span class="message">No bio set, just yet. Edit below!</span>
									@endif
									<form action="{{ route('edit-bio') }}" method="POST">
										@csrf
										<textarea name="bio" id="bio" cols="1" rows="8" required>Update your bio here...</textarea>
										<button type="submit" style="max-width: 160px; margin-left: 0px;">Submit</button>
									</form>
								</p>
							</div>
							<div class="content-tab" forTab="4">
								@if($history->count())
									<div class="content-block images pop-in">
										<div class="image-grid">
											<div class="row collapse small-up-2 medium-up-3 large-up-3">
												@foreach($history as $upload)
												<div class="columns">
													<div class="image-tile-container">
														<a href="/view-post/{{ $upload->id }}">
															<div class="image-tile" style="background-image: url('<?php echo asset('uploads/' . $upload->year . '/' . $upload->month . '/'. $upload->user_id . '/' . $upload->id . '.' . $upload->file_extension) ?>');"></div>
															<div class="image-text">
																<span class="title">{{ $upload->title }}</span>
																<span class="author">{{ $upload->author }}</span>
																<p class="desc">{{ $upload->description }}</p>
															</div>
														</a>
													</div>
												</div>
												@endforeach
											</div>
										</div>
									</div>
								@else
									<div class="row">
										<div class="columns">
											<span class="message">Your viewing history will appear here, but don't worry, only you can see it.</span>
										</div>
									</div>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="/js/home.js" defer></script>
@endsection