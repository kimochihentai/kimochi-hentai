@extends('layouts.site')
@section('body_id', 'feedback')
@section('body_class', 'site')
@section('content')
<div class="page-content">
	<div class="row">
		<div class="columns">
			<h1 class="page-heading">Feedback</h1>
			<p class="sub-text">
				Thanks in advance for getting this far; I thoroughly look forward to reading through whatever you'd like to send over this way! (Seriously, send <strong>anything</strong>) Feedback is <strong>anonymous</strong>, as you don't have to be signed in to send anything. Though if you really want to, feel free to attach your username or email if you'd like a <strong>response</strong>.
			</p>
			<form action="{{ route('post-feedback') }}" method="POST" id="post-feedback">
				@csrf
				<textarea name="feedback-message" id="" cols="1" rows="8" required>So like...</textarea>
				<button type="submit">Send</button>
			</form>
		</div>
	</div>
</div>
<script src="/js/home.js" defer></script>
@endsection