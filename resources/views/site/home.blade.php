@extends('layouts.site')
@section('body_id', 'home')
@section('body_class', 'site')
@section('content')
<div class="page-content">
	<div class="row">
		<div class="columns">
			<div class="content-block welcome-message">
				<h3>
					Welcome, friend!
				</h3>
				<p>
					Most adult sites run on ad revenue and subscriptions, <strong>Kimochi Hentai</strong> runs on <strong>raw perverted energy</strong>. This website began as a spontaneous desire to have a shot at building a hentai website, and well, now it's <strong>open for testing</strong>; crazy how time flies. Make yourself at home, and please use the <strong><a href="/feedback">feedback section</a></strong> in whatever way you like. I want your <strong>hate</strong>, your <strong>love</strong>, any <strong>bugs</strong> you notice, suggestions for <strong>future features</strong>, and <strong>everything</strong> in between.
				</p>
				<p>
					We're currently at <strong>{{ $total_users->count() }}</strong> users and counting. <strong><a href="/register">Register now</a></strong> if you wanna join these saucy perverts below and check out all the cool free user features. More to come!
				</p>
				<p>
					@foreach($total_users as $user)
					<strong><a href="/user/{{ $user->id }}">{{ $user->name }}</a></strong>,&nbsp;
					@endforeach
				</p>
			</div>
			<div class="content-block banner-slideshow slideshow">
				<?php $slide_count = 0; ?>
				@foreach($most_liked as $upload)
				<?php if($slide_count == 8) break; ?>
				<div class="slide">
					<a href="/view-post/{{$upload->id}}">
						<?php $background_image = 'uploads/' . $upload->year . '/' . $upload->month . '/'. $upload->user_id . '/' . $upload->id . '.' . $upload->file_extension ?>
						<div class="slideshow-image" style="background-image: url('<?php echo $background_image ?>');">
							<div class="image-text">
								<span class="title">{{ $upload->title }}</span>
								<span class="author">{{ $upload->author }}</span>
							</div>
						</div>
					</a>
				</div>
				<?php $slide_count++ ?>
				@endforeach
			</div>
		</div>
	</div>
	<div class="content-block featured-artists pop-in">
		<div class="row">
			<div class="columns">
				<h3>Top Artists</h3>
				<div class="row collapse small-up-3 align-center">
					<?php $artist_count = 0; ?>
					@foreach($top_artists as $top_artist)
					<?php if($artist_count == 3) break; ?>
					<div class="columns">
						<a href="/user/{{ $top_artist->id }}">
							<div class="artist">
								<?php
									$profile_picture_path = '/profile_pictures/placeholder.png';
									if(file_exists(public_path() . '/profile_pictures/' . $top_artist->id))
									{
										if(sizeof(scandir(public_path() . '/profile_pictures/' . $top_artist->id)) >= 3)
										{
											$profile_picture_path = '/profile_pictures/' . $top_artist->id . '/' . scandir(public_path() . '/profile_pictures/' . $top_artist->id)[2];
										}
									}
								?>
								<div class="image" style="background-image: url('<?php echo $profile_picture_path ?>"></div>
								<span class="name">{{ $top_artist->name }}</span>
							</div>
						</a>
					</div>
					<?php $artist_count++; ?>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="columns">
			<div class="content-block images pop-in">
				<h3>Most popular</h3>
				<div class="image-grid">
					<div class="row collapse small-up-2 medium-up-3 large-up-4">
						@foreach($most_popular as $upload)
						<div class="columns">
							<div class="image-tile-container">
								<a href="/view-post/{{ $upload->id }}">
									<div class="image-tile" style="background-image: url('<?php echo asset('uploads/' . $upload->year . '/' . $upload->month . '/'. $upload->user_id . '/' . $upload->id . '.' . $upload->file_extension) ?>');"></div>
									<div class="image-text">
										<span class="title">{{ $upload->title }}</span>
										<span class="author">{{ $upload->author }}</span>
										<p class="desc">{{ $upload->description }}</p>
									</div>
								</a>
							</div>
						</div>
						@endforeach
					</div>
					<a href="/gallery/most-popular">
						<button class="more">more</button>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="columns">
			<div class="content-block images pop-in">
				<h3>Newest</h3>
				<div class="image-grid">
					<div class="row collapse small-up-2 medium-up-3 large-up-4">
						@foreach($newest as $upload)
						<div class="columns">
							<div class="image-tile-container">
								<a href="/view-post/{{ $upload->id }}">
									<div class="image-tile" style="background-image: url('<?php echo asset('uploads/' . $upload->year . '/' . $upload->month . '/'. $upload->user_id . '/' . $upload->id . '.' . $upload->file_extension) ?>');"></div>
									<div class="image-text">
										<span class="title">{{ $upload->title }}</span>
										<span class="author">{{ $upload->author }}</span>
										<p class="desc">{{ $upload->description }}</p>
									</div>
								</a>
							</div>
						</div>
						@endforeach
					</div>
					<a href="/gallery/newest">
						<button class="more">more</button>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="/js/home.js" defer></script>
@endsection