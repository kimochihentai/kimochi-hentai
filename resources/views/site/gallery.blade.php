@extends('layouts.site')
@section('body_id', '')
@section('body_class', 'site gallery')
@section('content')
<div class="page-content">
	<div class="gallery">
		<div class="row">
			<div class="columns">
				@if(!$images->count())
				<div class="row">
					<div class="columns">
						<h3>Well, that's awkward. There don't seem to be any relevant posts here... yet...</h3>
					</div>
				</div>
				@else
				<div class="image-grid">
					<div class="row collapse small-up-2 medium-up-3 large-up-4">
						@if($images)
							@foreach($images as $image)
							<div class="columns">
								<div class="image-tile-container">
									<a href="/view-post/{{ $image->id }}">
										<div class="image-tile" style="background-image: url('<?php echo asset('uploads/' . $image->year . '/' . $image->month . '/'. $image->user_id . '/' . $image->id . '.' . $image->file_extension) ?>');"></div>
										<div class="image-text">
											<span class="title">{{ $image->title }}</span>
											<span class="author">{{ $image->author }}</span>
											<p class="desc">{{ $image->description }}</p>
										</div>
									</a>
								</div>
							</div>
							@endforeach
						@endif
					</div>
				</div>
				@endif
			</div>
		</div>
	</div>
</div>
<script src="/js/home.js" defer></script>
@endsection