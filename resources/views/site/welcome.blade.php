@extends('layouts.site')
@section('body_id', 'home')
@section('body_class', 'site')
@section('content')
<div class="page-content">
	<div class="content-block welcome-container">
		<style>
			.content-block.welcome-container::before
			{
				background-image: url('https://i.imgur.com/ZYjBCmK.jpg');
			}
		</style>
		<div class="inner">
			<h2 class="message">
				We are thoroughly pleased to be graced with your presence, <span class="name">{{ Auth::user()->name }}</span>!<br><br>Thank you for joining our forces. 
			</h2>
			<ul class="dialogue-options">
				<li>
					<a href="/" class="begin-journey-link">
						"Enough talk, hentai awaits!
					</a>
				</li>
				<li>
					<a href="/" class="begin-journey-link">
						"Joining forces? Look buddy, I came here to fap, no promises."
					</a>
				</li>
				<li>
					<a href="/" class="begin-journey-link">
						"It will be an honour to fight beside you in the name of illustrated pornography, my lord."
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<script src="/js/home.js" defer></script>
@endsection