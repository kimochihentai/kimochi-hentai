@extends('layouts.site')
@section('body_id', 'post-page')
@section('body_class', 'site')
@section('content')
<div class="page-content">
	<div class="post-main">
		<div class="row expanded collapse">
			<div class="columns">
				<img src="<?php echo asset('uploads/' . $post_data->post->year . '/' . $post_data->post->month . '/'. $post_data->post->user_id . '/' . $post_data->post->id . '.' . $post_data->post->file_extension) ?>" alt="">
			</div>
		</div>
	</div>
	<div class="post-bar">
		<div class="row expanded align-middle">
			<div class="columns">
				<div class="views"><span>Views:</span><span>{{ $post_data->post->views }}</span></div>
			</div>
			<div class="columns">
				<div class="title-container">
					<span class="title">{{ $post_data->post->title }}</span>
					<a href="/user/{{ $post_data->post->user_id }}"><span class="author">{{ $post_data->post->author }}</span></a>
				</div>
			</div>
			<div class="columns">
				<div class="likes-container">
					@if(!$post_data->liked)
					<a href="/like-post/{{ $post_data->post->id }}">
						<button class="like">
					@else
					<a href="/unlike-post/{{ $post_data->post->id }}">
						<button class="like active">
					@endif
							<div class="icon">
								<i class="far fa-heart"></i>
								<i class="fas fa-heart"></i>
							</div>
							<span class="value">{{ $post_data->post->likes }}</span>
						</button>
					</a>
				</div>
				<div class="flags-container">
					@if(!$post_data->flagged)
					<a href="/flag-post/{{ $post_data->post->id }}">
						<button class="flag">
					@else
					<a href="/unflag-post/{{ $post_data->post->id }}">
						<button class="flag active">
					@endif
							<div class="icon">
								<i class="far fa-flag"></i>
								<i class="fas fa-flag"></i>
							</div>
							<span class="value">{{ $post_data->post->flags }}</span>
						</button>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="post-lower">
		<div class="row">
			<div class="columns shrink">
				<?php
					$profile_picture_path = '/profile_pictures/placeholder.png';
					if(file_exists(public_path() . '/profile_pictures/' . $post_data->post->user_id))
					{
						if(sizeof(scandir(public_path() . '/profile_pictures/' . $post_data->post->user_id)) >= 3)
						{
							$profile_picture_path = '/profile_pictures/' . $post_data->post->user_id . '/' . scandir(public_path() . '/profile_pictures/' . $post_data->post->user_id)[2];
						}
					}
				?>
				<a href="/user/{{ $post_data->post->user_id }}">
					<div class="profile-picture easy-edit-container" style="background-image: url('{{ $profile_picture_path }}')"></div>
				</a>
			</div>
			<div class="columns">
				<div class="description">
					<p>
						{{ $post_data->post->description }}
					</p>
				</div>
				<div class="tags">
					@foreach($post_data->tags as $tag)
					<a href="/gallery/tag/{{ $tag->id }}">
						<span class="tag">{{ $tag->name }}</span>
					</a>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="columns">
			<div class="comments">
				<div class="comment-list">
					@if($comments->count())
						@foreach($comments as $comment)
							@if($comment->user_id == $post_data->post->user_id)
							<div class="comment-block uploader">
								<div class="row collapse">
									<div class="columns">
										<div class="comment">
											<span>
												{{ $comment->text }}
											</span>
										</div>
									</div>
									<div class="columns shrink">
										<a href="/user/{{$comment->user_id}}">
											<div class="author-pic" style="background-image: url('{{ $profile_picture_path }}')"></div>
										</a>
									</div>
								</div>
							</div>
							@else
							<div class="comment-block">
								<div class="row collapse">
									<div class="columns shrink">
										<a href="/user/{{$comment->user_id}}">
											<?php  
												$commenter_picture_path = '/profile_pictures/placeholder.png';
												if(file_exists(public_path() . '/profile_pictures/' . $comment->user_id))
												{
													if(sizeof(scandir(public_path() . '/profile_pictures/' . $comment->user_id)) >= 3)
													{
														$commenter_picture_path = '/profile_pictures/' . $comment->user_id . '/' . scandir(public_path() . '/profile_pictures/' . $comment->user_id)[2];
													}
												}
											?>
											<div class="author-pic" style="background-image: url('{{ $commenter_picture_path }}')"></div>
										</a>
									</div>
									<div class="columns">
										<div class="comment">
											<span>
												{{ $comment->text }}
											</span>
										</div>
									</div>
								</div>
							</div>
							@endif
						@endforeach
					@else
						<h3>No comments yet!</h3>
					@endif
				</div>
				<div class="comment-input">
					<div class="row collapse">
						<div class="columns">
							<form action="{{ route('post-comment') }}" method="POST">
								@csrf
								<input type="text" name="post_id" style="display: none !important;" value="{{ $post_data->post->id }}">
								<textarea name="comment" id="" cols="1" rows="8">I love it!</textarea>
								<button type="submit"><i class="far fa-paper-plane"></i></button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="/js/home.js" defer></script>
<!-- <script src="../node_modules/countup.js/dist/countUp.min.js" defer></script> -->
@endsection