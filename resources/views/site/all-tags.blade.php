@extends('layouts.site')
@section('body_id', 'all-tags')
@section('body_class', 'site')
@section('content')
<div class="page-content">
	<div class="tag-block-list">
		<div class="row large-up-6 collapse">
			@foreach($tags as $tag)
			<div class="columns">
				<a href="/gallery/tag/{{$tag->id}}">
					<div class="tag-block">
						<span class="name">{{ $tag->name }}</span>
						<span class="count">{{ $tag->posts }}</span>
					</div>
				</a>
			</div>
			@endforeach
		</div>
	</div>
</div>
<script src="/js/home.js" defer></script>
@endsection