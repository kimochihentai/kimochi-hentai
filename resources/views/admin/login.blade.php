@extends('layouts.admin')
@section('bodyId', 'login')
@section('content')
<div class="page-content">
	<div class="form-page-container">
		<div class="row">
			<div class="columns">
				<img src="" alt="Kimochi" style="display: block; width: 240px; height: 80px; margin: auto; margin-bottom: 24px; background: #eee;">
				<form action="" method="POST">
					@csrf
					<label for="username">Username</label>
					<input type="text" name="username" id="username" required>
					<label for="password">Password</label>
					<input type="text" name="password" id="password" required>
					<button type="submit">Submit</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection