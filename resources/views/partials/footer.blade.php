<footer>
	<div>
		<span class="copy-1">
			Website Copyright &copy; Kimochi 2018
		</span>
	</div>
	<div>
		<span class="copy-2">
			All artworks belong to their respective owners.
		</span>
	</div>
</footer>