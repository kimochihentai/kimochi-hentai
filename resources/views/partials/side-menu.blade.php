<div class="side-menu">
	<div class="inner">
		@if(Auth::check())
		<div class="greeting">
			<span>Ahoy there, <strong>{{ Auth::user()->name }}!</strong></span>
		</div>
		@endif
		<ul>
			<li>
				<div class="search-container">
					<form action="{{ route('search') }}" method="POST">
						@csrf
						<input type="text" name="search" class="search" placeholder="Some hentai, perhaps?"><i class="fas fa-search"></i>
					</form>
				</div>
			</li>
			@guest
			<!-- sign in -->
			<li>
				<a href="/login">Sign in</a>
			</li>
			<li>
				<a href="/register">Register</a>
			</li>
			@endguest
			@auth
			<!-- my account -->
			<li>
				<a href="/my-account">My Account</a>
			</li>
			<li>
				<a href="/logout">Logout</a>
			</li>
			@endauth
			<li>
				<a href="/gallery/most-popular">Most Popular</a>
			</li>
			<li>
				<a href="/gallery/newest">Newest</a>
			</li>
			<li>
				<a href="/all-tags">All Tags</a>
			</li>
			<li>
				<a href="/feedback">Feedback</a>
			</li>
		</ul>
	</div>
</div>