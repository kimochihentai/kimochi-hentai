<!DOCTYPE html>
	<!--[if lte IE 6]><html class="preIE7 preIE8 preIE9"><![endif]-->
	<!--[if IE 7]><html class="preIE8 preIE9"><![endif]-->
	<!--[if IE 8]><html class="preIE9"><![endif]-->
	<!--[if gte IE 9]><!--><html><!--<![endif]-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<title>Kimochi</title>
		<meta name="author" content="Alex Nirenovic">
		<meta name="description" content="description here">
		<meta name="keywords" content="keywords,here">
		<link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon">
		<link rel="stylesheet" href="">
		<!-- Foundation -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.4.3/css/foundation.min.css">
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.4.3/js/plugins/foundation.util.mediaQuery.min.js">
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.2.1/foundation-flex.min.css">
    	<!-- Google Fonts -->
    	<link href="https://fonts.googleapis.com/css?family=Exo:700,900" rel="stylesheet">
    	<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
		<!-- Slick Carousel -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">
    	<!-- app.css -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
		<!-- favicon -->
    	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
	</head>
	<body class="@yield('body_class')" id="@yield('body_id')">
	    @yield('content')
	    <!-- Jquery -->
	    <script
	      src="https://code.jquery.com/jquery-3.3.1.min.js"
	      integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	      crossorigin="anonymous"></script>
	    <!-- ScrollTo.js -->
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.2/jquery.scrollTo.min.js"></script>
	    <!-- Typed.js -->
	    <!-- <script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.9"></script> -->
	    <!-- Slick.js -->
	    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	    <!-- JS Cookie -->
	    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
	    <!-- Custom scripts -->
	    <script src="/js/misc.js"></script>
	    <script src="/js/scroll.js"></script>
	</body>
</html>