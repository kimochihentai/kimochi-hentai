<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// AUTH
Route::get('/login', 'Auth\LoginController@index');
Route::get('/logout', 'Auth\LoginController@logout');
Auth::routes();

// SITE
// home
Route::get('/', 'Site\HomeController@index');
// welcome page
Route::get('/welcome', 'Site\WelcomeController@index');
// feedback
Route::get('/feedback', 'Site\FeedbackController@index');
Route::post('/post-feedback', 
		   ['as' => 'post-feedback',
		    'uses' => 'Site\FeedbackController@postFeedback']);


// MY ACCOUNT
// =========================================================================================
Route::get('/my-account', 'Site\MyAccountController@index');
// edit profile picture
Route::post('/my-account/update-profile-picture', 
		   ['as' => 'update-profile-picture', 
	  		'uses' =>'Site\MyAccountController@updateProfilePicture']);
// edit patreon link
Route::post('/my-account/update-patreon-link', 
		   ['as' => 'update-patreon-link', 
	  		'uses' =>'Site\MyAccountController@updatePatreonLink']);
// edit twitter link
Route::post('/my-account/update-twitter-link', 
		   ['as' => 'update-twitter-link', 
	  		'uses' =>'Site\MyAccountController@updateTwitterLink']);
// edit facebook link
Route::post('/my-account/update-facebook-link', 
		   ['as' => 'update-facebook-link', 
	  		'uses' =>'Site\MyAccountController@updateFacebookLink']);
// upload post
Route::post('/my-account/upload',
		   ['as' =>'upload-post',
			'uses' => 'Site\MyAccountController@uploadPost']);
// edit bio
Route::post('/my-account/edit-bio', 
		   ['as' => 'edit-bio',
			'uses' => 'Site\MyAccountController@editBio']);
// delete post
Route::get('/my-account/delete-post/{post_id}', 'Site\MyAccountController@deletePost');
// User page
Route::get('/user/{user_id}', 'Site\UserPageController@index');
// report user
Route::get('/report-user/{user_id}', 'Site\UserPageController@reportUser');



// POST PAGE
// =========================================================================================
// view post
Route::get('/view-post/{post_id}', 'Site\PostPageController@index');
// Like a post
Route::get('/like-post/{post_id}', 'Site\PostPageController@like');
Route::get('/unlike-post/{post_id}', 'Site\PostPageController@unlike');
// post comment
Route::post('/post-comment',
		   ['as' => 'post-comment',
			'uses' => 'Site\PostPageController@postComment']);
// Flag a post
Route::get('/flag-post/{post_id}', 'Site\PostPageController@flag');
Route::get('/unflag-post/{post_id}', 'Site\PostPageController@unflag');




// GALLERY
// =========================================================================================
// gallery for Most Popular
Route::get('/gallery/most-popular', 'Site\GalleryController@mostPopular');
// gallery for Newest
Route::get('/gallery/newest', 'Site\GalleryController@newest');
// gallery for specific tag
Route::get('/gallery/tag/{tag_id}', 'Site\GalleryController@viewTag');
// All tags view
Route::get('/all-tags', 'Site\AllTagsController@index');
// search
Route::post('/gallery/search-results',
		   ['as' => 'search',
			'uses' => 'Site\GalleryController@search']);


// code red
Route::get('/yabungus', 'Site\MyAccountController@yabungus');