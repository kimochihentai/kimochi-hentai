<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\DB;

class UserPageController extends Controller
{
    public function index($user_id)
    {
    	if(Auth::check() && $user_id == Auth::user()->id)
		{
			return redirect('/my-account');
		}
		else
		{
	    	// $profile_pictures_path = str_replace('\\', '/', public_path('storage/profile_pictures/'));
			$profile_pic_dir = public_path('/profile_pictures');
			$user_details = new \stdClass();
			$user_details->profile_picture = 0;
            if(!file_exists($profile_pic_dir . '/' . $user_id))
            {
                mkdir($profile_pic_dir . '/' . $user_id);
            }
            if(sizeof(scandir($profile_pic_dir . '/' . $user_id)) >= 3)
            {
                if($profile_pic_dir . '/' . $user_id . '/' . scandir($profile_pic_dir . '/' . $user_id)[2] != null)
                {
                    $user_details->profile_picture = scandir($profile_pic_dir . '/' . $user_id)[2];
                }
            }

	        $user_details->user = DB::table('users')->where('id', $user_id)->first();
	        $uploads = DB::table('posts')->where('user_id', $user_id)->get();
	        $liked_posts = DB::table('likes')->where('likes.user_id', '=', $user_id)->join('posts', 'likes.post_id', '=', 'posts.id')->get();
	        $reported = false;

	        if(Auth::check())
        	{
		        if(DB::table('reports')->where('user_id', $user_id)->where('reporter_id', Auth::user()->id)->first())
	        	{
	        		$reported = true;
	        	}
        	}

			return view('site.user')
	            ->with('user_details', $user_details)
	            ->with('uploads', $uploads)
	            ->with('liked_posts', $liked_posts)
	            ->with('reported', $reported);
		}
    }

    public function reportUser($user_id)
    {
    	if(Auth::check())
		{
			if(!DB::table('reports')->where('user_id', $user_id)->where('reporter_id', Auth::user()->id)->first())
			{
		    	// add new report record
		    	DB::table('reports')->insert([
		    		'user_id' => $user_id,
		    		'reporter_id' => Auth::user()->id
		    	]);
		    	// increment reports of reported user
		    	DB::table('users')->where('id', $user_id)->increment('reports');
			}

	    	return redirect()->back();
		}
		else
		{
			return redirect('/login');
		}
    }
}
