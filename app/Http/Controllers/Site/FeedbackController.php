<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Auth;

class FeedbackController extends Controller
{
    public function index()
    {
    	return view('site.feedback');
    }

    public function postFeedback(Request $request)
    {
    	DB::table('feedback')->insert([
    		'message' => $request->input('feedback-message')
    	]);

    	return redirect()->back();
    }
}
