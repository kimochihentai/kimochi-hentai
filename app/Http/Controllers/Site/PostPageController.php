<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Auth;

class PostPageController extends Controller
{
    public function index(Request $request, $post_id)
    {
    	// return back if no such post exists
    	if(DB::table('posts')->where('id', $post_id)->first() == null)
		{
			return back();
		}

    	$post_data = new \stdClass();

    	// check if this is the first time a particular user is viewing the post by IP address
    	$user_ip = \Request::ip();
    	if(DB::table('views')->where('ip_address', $user_ip)->where('post_id', $post_id)->first() == null)
		{
			DB::table('views')->insert([
				'ip_address' => $user_ip,
				'post_id' => $post_id
			]);
			// increment views for the corresponding post since not existing IP address-based viewer
			DB::table('posts')->where('id', $post_id)->increment('views');
		}
    	
    	// fetch the corresponding post
    	$post_data->post = DB::table('posts')->where('id', $post_id)->get()[0];

    	// track if post is already liked or flagged by corresponding user
		$post_data->liked = 0;
		$post_data->flagged = 0;
    	if(Auth::check())
		{
	    	if(DB::table('likes')->where('post_id', $post_id)->where('user_id', Auth::user()->id)->first() != null)
			{
				$post_data->liked = 1;
			}
			if(DB::table('flags')->where('post_id', $post_id)->where('flagger_id', Auth::user()->id)->first() != null)
			{
				$post_data->flagged = 1;
			}
		}

		// get related tags
		$post_data->tags = DB::table('tagged_posts')->where('post_id', $post_id)->join('tags', 'tagged_posts.tag_id', '=', 'tags.id')->orderBy('name')->get();
		// get related comments
		$comments = DB::table('comments')->where('post_id', $post_id)->get();

    	return view('site.post-page')
    		->with('post_data', $post_data)
    		->with('comments', $comments);
    }

    public function like(Request $request, $post_id)
    {
    	if(Auth::check())
		{
			if(!DB::table('likes')->where('post_id', $post_id)->where('user_id', Auth::user()->id)->first())
			{
		    	// increment likes of author
		    	DB::table('users')->where('id', DB::table('posts')->where('id', $post_id)->first()->user_id)->increment('likes');
		    	// increment likes of post
		    	DB::table('posts')->where('id', $post_id)->increment('likes');
		    	// create new record of like instance
		    	DB::table('likes')->insert([
		    		'post_id' => $post_id,
		    		'user_id' => Auth::user()->id
		    	]);
			}

	    	return redirect()->back();
		}
		else
		{
			return redirect('/login');
		}
    }

    public function unlike(Request $request, $post_id)
    {
    	if(Auth::check())
		{
			if(DB::table('likes')->where('post_id', $post_id)->where('user_id', Auth::user()->id)->first())
			{
		    	// decrement likes of author
		    	DB::table('users')->where('id', DB::table('posts')->where('id', $post_id)->first()->user_id)->decrement('likes');
		    	// decrement likes of post
		    	DB::table('posts')->where('id', $post_id)->decrement('likes');
		    	// delete record of like
		    	DB::table('likes')->where('user_id', Auth::user()->id)->where('post_id', $post_id)->delete();
			}

	    	return redirect()->back();
		}
		else
		{
			return redirect('/login');
		}
    }

    public function flag(Request $request, $post_id)
    {
    	if(Auth::check())
		{
			if(!DB::table('flags')->where('post_id', $post_id)->where('flagger_id', Auth::user()->id)->first())
			{
		    	// increment flags of post
		    	DB::table('posts')->where('id', $post_id)->increment('flags');
		    	// create new record of like instance
		    	DB::table('flags')->insert([
		    		'post_id' => $post_id,
		    		'flagger_id' => Auth::user()->id
		    	]);
			}

	    	return redirect()->back();
		}
		else
		{
			return redirect('/login');
		}
    }

    public function unflag(Request $request, $post_id)
    {
    	if(Auth::check())
		{
			if(DB::table('flags')->where('post_id', $post_id)->where('flagger_id', Auth::user()->id)->first())
			{
		    	// decrement flags of post
		    	DB::table('posts')->where('id', $post_id)->decrement('flags');
		    	// delete record of flag
		    	DB::table('flags')->where('flagger_id', Auth::user()->id)->where('post_id', $post_id)->delete();
			}

	    	return redirect()->back();
		}
		else
		{
			return redirect('/login');
		}
    }

    public function postComment(Request $request)
    {
    	if(Auth::check())
		{
	    	DB::table('comments')->insert([
	    		'user_id' => Auth::user()->id,
	    		'post_id' => $request->input('post_id'),
	    		'text' => $request->input('comment')
	    	]);
			
    		return redirect()->back();
		}
		else
		{
			return redirect('/login');
		}
    }
}
