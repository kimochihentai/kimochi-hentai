<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class WelcomeController extends Controller
{
    public function index()
    {
    	if(Auth::check())
    	{
    		return view('site.welcome');
    	}
    	else
    	{
    		return redirect('/login');
    	}
    }
}
