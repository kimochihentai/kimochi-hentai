<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
    	$most_liked = DB::table('posts')->orderBy('likes', 'desc')->get();
    	$top_artists = DB::table('users')->orderBy('likes', 'desc')->get();
    	$most_popular = DB::table('posts')->orderBy('views', 'desc')->get();
    	$newest = DB::table('posts')->orderBy('id', 'desc')->get();
    	$total_users = DB::table('users')->get();

    	return view('site.home')
    		->with('most_liked', $most_liked)
    		->with('newest', $newest)
    		->with('most_popular', $most_popular)
    		->with('top_artists', $top_artists)
    		->with('total_users', $total_users);
    }
}
