<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class MyAccountController extends Controller
{
    public function index()
    {
    	if(Auth::check())
    	{
    		// $profile_pictures_path = str_replace('\\', '/', public_path('storage/profile_pictures/'));
    		$profile_pic_dir = public_path('profile_pictures');
            $user_details = new \stdClass();
            $user_details->profile_picture = 0;
            if(!file_exists($profile_pic_dir . '/' . Auth::user()->id))
            {
                mkdir($profile_pic_dir . '/' . Auth::user()->id);
            }
            if(sizeof(scandir($profile_pic_dir . '/' . Auth::user()->id)) >= 3)
            {
                if($profile_pic_dir . '/' . Auth::user()->id . '/' . scandir($profile_pic_dir . '/' . Auth::user()->id)[2] != null)
                {
                    $user_details->profile_picture = scandir($profile_pic_dir . '/' . Auth::user()->id)[2];
                }
            }

    		$tags = DB::table('tags')->orderBy('name')->get();
            $uploads = DB::table('posts')->where('user_id', Auth::user()->id)->get();
            $liked_posts = DB::table('likes')->where('likes.user_id', '=', Auth::user()->id)->join('posts', 'likes.post_id', '=', 'posts.id')->get();
            $history = DB::table('views')->where('ip_address', \Request::ip())->join('posts', 'views.post_id', '=', 'posts.id')->get();
    		return view('site.my-account')
                ->with('user', $user_details)
                ->with('tags', $tags)
                ->with('uploads', $uploads)
                ->with('liked_posts', $liked_posts)
                ->with('history', $history);
    	}

    	else
    	{
    		return redirect('/login');
    	}
    }

    public function updateProfilePicture(Request $request)
    {
    	if ($request->hasFile('upload')) 
        {
            $profile_pic_dir = public_path('/profile_pictures/') . $request->user()->id;
            
            if(is_dir($profile_pic_dir))
            {
                foreach(scandir($profile_pic_dir) as $profile_pic)
                {
                    if($profile_pic != '.' && $profile_pic != '..')
                    {
                        unlink($profile_pic_dir . '/' . $profile_pic);
                    }
                    else
                    {
                        // echo $profile_pic;
                    }
                }
            }
            $file = $request->file('upload');
            $extension = $file->getClientOriginalExtension();
            $filename = $request->user()->id . '.' . $extension;
            $file->move(public_path('/profile_pictures/' . $request->user()->id), $filename);
        }
        else
        {
        }

		return redirect('/my-account');
    }

    public function updatePatreonLink(Request $request)
    {
        DB::table('users')
            ->where('id', Auth::user()->id)
            ->update(['social_link_patreon' => $request->input('link-patreon')]);

    	return MyAccountController::index();
    }

    public function updateTwitterLink(Request $request)
    {
    	DB::table('users')
            ->where('id', Auth::user()->id)
            ->update(['social_link_twitter' => $request->input('link-twitter')]);

        return MyAccountController::index();
    }

    public function updateFacebookLink(Request $request)
    {
    	DB::table('users')
            ->where('id', Auth::user()->id)
            ->update(['social_link_facebook' => $request->input('link-facebook')]);

        return MyAccountController::index();
    }

    public function uploadPost(Request $request)
    {
        $file = $request->file('upload');
        $title = $request->input('title');
        $desc = $request->input('description');

        DB::table('posts')->insert([
            'title' => $title,
            'user_id' => Auth::user()->id,
            'author' => Auth::user()->name,
            'description' => $desc,
            'year' => date('Y'),
            'month' => date('m'),
            'likes' => 0,
            'views' => 0,
            'file_extension' => $file->getClientOriginalExtension()
        ]);

        $uploads_dir = public_path('/uploads/');

        // if new year
        if(!file_exists($uploads_dir . date('Y')))
        {
            mkdir($uploads_dir . date('Y'));
        }

        $uploads_dir = $uploads_dir . date('Y') . '/';

        // if new month of year
        if(!file_exists($uploads_dir . date('m')))
        {
            mkdir($uploads_dir . date('m'));
        }

        $uploads_dir = $uploads_dir . date('m') . '/';

        // uploads will be archived into folders named after corresponding user ID
        if(!file_exists($uploads_dir . Auth::user()->id))
        {
            mkdir($uploads_dir . Auth::user()->id);
        }

        $uploads_dir = $uploads_dir . Auth::user()->id;

        // store file
        // $file->storeAs('public/uploads/' . date('Y') . '/' . date('m') . '/' . Auth::user()->id, 
                       // $title . '.' . $file->extension());
        // $file->move(public_path('/profile_pictures/' . $request->user()->id), $filename);
        $extension = $file->getClientOriginalExtension();
        $filename = DB::table('posts')->where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first()->id . '.' . $extension;
        $file->move($uploads_dir, $filename);

        foreach($request->input('tags') as $tag)
        {
            // create new post-tag relationship record
            DB::table('tagged_posts')->insert([
                'post_id' => DB::table('posts')->where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first()->id,
                'tag_id' => DB::table('tags')->where('name', $tag)->first()->id
            ]);
            // increment corresponding tag upload count
            DB::table('tags')->where('name', $tag)->increment('posts');
        }

        return redirect('/my-account');
    }

    public function deletePost($post_id)
    {
        // remove likes of post from user's total likes
        $post_likes = DB::table('posts')->where('id', $post_id)->first()->likes;
        $current_likes = DB::table('users')->where('id', Auth::user()->id)->first()->likes;
        $updated_likes = $current_likes - $post_likes;
        DB::table('users')->where('id', Auth::user()->id)->update(['likes' => $updated_likes]);
        // remove dependent records first
        DB::table('views')->where('post_id', $post_id)->delete();
        DB::table('likes')->where('post_id', $post_id)->delete();
        DB::table('tagged_posts')->where('post_id', $post_id)->join('tags', 'tagged_posts.tag_id', '=', 'tags.id')->decrement('tags.posts');
        DB::table('tagged_posts')->where('post_id', $post_id)->delete();
        DB::table('flags')->where('post_id', $post_id)->delete();
        DB::table('comments')->where('post_id', $post_id)->delete();
        // delete post image
        $post = DB::table('posts')->where('id', $post_id)->first();
        if(file_exists(public_path('/uploads/' . $post->year . '/' . $post->month . '/' . $post->user_id . '/' . $post->id . '.' . $post->file_extension)))
        {
            unlink(public_path('/uploads/' . $post->year . '/' . $post->month . '/' . $post->user_id . '/' . $post->id . '.' . $post->file_extension));
        }
        // lastly, delete post record
        DB::table('posts')->where('id', $post_id)->delete();

        return redirect('/my-account');
    }

    public function editbio(Request $request)
    {
        DB::table('users')
            ->where('id', Auth::user()->id)
            ->update(['bio' => $request->input('bio')]);

        return redirect('/my-account');
    }

    public function yabungus()
    {
        if(Auth::user()->email == 'a.nirenovic@gmail.com')
        {
            // clear posts
            $uploads_path = public_path() . '/uploads/';
            $uploads_dir = scandir($uploads_path);
            foreach($uploads_dir as $year_dir)
            {
                if($year_dir != '.' && $year_dir != '..')
                {
                    $year_dir = $uploads_path . '/' . $year_dir;
                    foreach(scandir($year_dir) as $month_dir)
                    {
                        if($month_dir != '.' && $month_dir != '..')
                        {
                            $month_dir = $year_dir . '/' . $month_dir;
                            foreach(scandir($month_dir) as $user_dir)
                            {
                                if($user_dir != '.' && $user_dir != '..')
                                {
                                    $user_dir = $month_dir . '/' . $user_dir;
                                    foreach(scandir($user_dir) as $upload)
                                    {
                                        if($upload != '.' && $upload != '..')
                                        {
                                            unlink($user_dir . '/' . $upload);
                                        }
                                    }
                                    rmdir($user_dir);
                                }
                            }
                        }
                    }
                }
            }

            // reset posts count column in tags table
            DB::table('tags')->update(['posts' => 0]);
            // clear views table
            DB::table('views')->delete();
            // clear tagged_posts table
            DB::table('tagged_posts')->delete();
            // clear likes table
            DB::table('likes')->delete();
            // clear flags table
            DB::table('flags')->delete();
            // clear reports table
            DB::table('reports')->delete();
            // clear comments table
            DB::table('comments')->delete();
            // clear posts table
            DB::table('posts')->delete();
            // reset total likes of users
            DB::table('users')->update(['likes' => 0]);
        }
        
        return redirect('/');
    }
}