<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class GalleryController extends Controller
{
    public function index()
    {
    	// probably unused
    }

    public function mostPopular()
    {
    	$most_popular = DB::table('posts')->orderBy('views', 'desc')->get();

    	return view('site.gallery')->with('images', $most_popular);
    }

    public function newest()
    {
    	$newest = DB::table('posts')->orderBy('id', 'desc')->get();

    	return view('site.gallery')->with('images', $newest);
    }

    public function viewTag($tag_id)
    {
    	$related_posts = DB::table('tagged_posts')->where('tag_id', $tag_id)->join('posts', 'tagged_posts.post_id', '=', 'posts.id')->get();

    	return view('site.gallery')->with('images', $related_posts);
    }

    public function search(Request $request)
    {
    	$query = $request->input('search');
    	$query_parts = explode(" ", $query);
    	$num_parts = sizeof($query_parts);
    	$results_by_tag = null;
    	$results_by_title = null;
    	$results_by_user = null;

    	// if only searching for a tag/user/title etc
    	if($num_parts == 1)
    	{
    		// if tag
    		if(DB::table('tags')->where('name', $query_parts[0])->first())
			{
				$matched_tag = DB::table('tags')->where('name', $query_parts[0])->first();

				return redirect('/gallery/tag/' . $matched_tag->id);
			}
			// if title
			if(DB::table('posts')->where('title', $query_parts[0])->first())
			{
				if(DB::table('posts')->where('title', $query_parts[0])->get()->count() > 1)
				{
					$empty = false;
					return view('site.gallery')
						->with('empty', $empty)
						->with('images', DB::table('posts')->where('title', $query_parts[0])->get())
						->with('results_by_tag', $results_by_tag)
			    		->with('results_by_title', $results_by_title)
			    		->with('results_by_user', $results_by_user);
				}
				else
				{
					return redirect('/view-post/' . DB::table('posts')->where('title', $query_parts[0])->first()->id);
				}
			}
			// if a single username
			if(DB::table('users')->where('name', $query_parts[0])->first())
			{
				return redirect('/user/' . DB::table('users')->where('name', $query_parts[0])->first()->id);
			}
    	}

    	$results_by_tag = DB::table('tags')->whereIn('name', $query_parts)->join('tagged_posts', 'tags.id', '=', 'tagged_posts.id')->join('posts', 'tagged_posts.id', '=', 'posts.id')->get();
    	$results_by_title = DB::table('posts')->whereIn('title', $query_parts)->get();
    	// $results_by_user = DB::table('posts')->whereIn('author', $query_parts)->get();
    	$results_by_user = DB::table('users')->whereIn('name', $query_parts);
    	$images = null;
    	$empty = false;

    	if($results_by_tag->count() == 0 && $results_by_title->count() == 0 && $results_by_user->count() == 0)
    	{
    		$empty = true;
    	}

    	if($empty)
    	{
    		return view('site.gallery')->with('empty', $empty);
    	}
    	else
    	{
	    	return view('site.gallery')
	    		->with('images', $images)
	    		->with('results_by_tag', $results_by_tag)
	    		->with('results_by_title', $results_by_title)
	    		->with('results_by_user', $results_by_user);
    	}

    	// TO-DO: run checks on $num_parts length and improve results based on type of search. For example, if the part 
    	// 		  length of the query is only one, the user is clearly only searching for a single tag, artist or title. 
    	// 		  In this case, it would be more convenient to just jump to the corresponding user/post/tag related page,
    	// 		  rather than returning a gallery list with a potentially minimal set of results.
    }
}
