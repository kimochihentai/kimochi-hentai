<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AllTagsController extends Controller
{
    public function index()
    {
    	$tag_list = DB::table('tags')->orderBy('name')->get();

    	return view('site.all-tags')->with('tags', $tag_list);
    }
}
