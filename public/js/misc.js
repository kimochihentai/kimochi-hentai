$('.menu-button').on('click', function(){
	$(this).toggleClass('active');
	$('.side-menu').toggleClass('active');
});

$('.easy-edit-container button.easy-edit-button').on('click', function(){
    $(this).toggleClass('active');
});

$('.profile-nav a').on('click', function(e){
    e.preventDefault();
    var selected_id = $(this).attr('tabFor');
    $('.profile-nav a').each(function(){
        $(this).removeClass('active');
    });
    $(this).addClass('active');
    $('.profile-nav-content .content-tab').each(function(){
        // alert($(this).attr('forTab'));
        // alert(selected_id);
        $(this).removeClass('active');
        if($(this).attr('forTab') == selected_id)
        {
            $(this).addClass('active');
        }
    });
});

$('button.upload').on('click', function(){
    $('.upload-post-overlay').addClass('active');
});
$('.upload-post-overlay .close-overlay-button').on('click', function(){
    $('.upload-post-overlay').removeClass('active');
});

$('.tag-list .tag-item').on('click', function(){
    $(this).toggleClass('active');
    if($(this).hasClass('active'))
    {
        $(this).find('input[type="checkbox"]').prop('checked', true);
    }
    else
    {
        $(this).find('input[type="checkbox"]').prop('checked', false);
    }
});
$('.tag-list .tag-item label').on('click', function(e){
    e.preventDefault();
});

// tab key menu toggle
$(document).keydown(function(e) {
    var code = e.keyCode || e.which;
    if (code === 9) {  
        e.preventDefault();
        $('.menu-button').toggleClass('active');
        $('.side-menu').toggleClass('active');
        if($('.side-menu').hasClass('active'))
        {
        	$('.side-menu .search').focus();
        }
    }
});

$(document).ready(function(){
    if($('.post-main').find('img').width() >= $('.post-main').find('img').height() * 1.2)
    {
        $('.post-main').addClass('landscape');

        if($('.post-main').find('img').height() >= $(window).height())
        {
            $('.post-main').addClass('limited-width');
        }
        if($('.post-main').find('img').width() < $(window).width())
        {
            $('.post-main').addClass('short-width');
        }
    }
    else
    {
        $('.post-main').addClass('portrait');
    }
});

$('.image-tile-container .delete-post-button').on('click', function(){
    var post_id = $(this).attr('forPost');
    $('.confirm-delete-post-overlay a.button.confirm').attr('href', '/my-account/delete-post/' + post_id);
    $('.confirm-delete-post-overlay').addClass('active');
});

$('.confirm-delete-post-overlay a.button.cancel').on('click', function(e){
    e.preventDefault();
    $(this).closest('.confirm-delete-post-overlay').removeClass('active');
})