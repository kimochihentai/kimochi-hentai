$(document).ready(function(){
	$('.banner-slideshow').slick({
 		infinite: true,
 		slidesToShow: 1,
 		slidesToScroll: 1,
 		autoplay: true,
 		autoplaySpeed: 2000,
 		dots: true,
 		arrows: true,
 		prevArrow: '<button type="button" class="slick-prev"><div class="arrow-inner"></div></button>',
 		nextArrow: '<button type="button" class="slick-next"><div class="arrow-inner"></div></button>'
	});
});