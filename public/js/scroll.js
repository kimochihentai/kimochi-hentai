function updatePopIn()
{
	$('.pop-in').each(function(){
		if($(this).offset().top < $(document).scrollTop() + ($(window).height() * 0.8))
		{
			$(this).addClass('popped');
		}
	});
}

function checkScrollEvents()
{
	updatePopIn();
}

$(window).on('scroll', function(){
	checkScrollEvents()
});

// run scroll event checks once before any scroll
$(document).ready(function(){
	checkScrollEvents();
});